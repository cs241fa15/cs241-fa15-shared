#include <stdio.h>
#include <string.h>
#include <unistd.h>

int main() {
  puts("Hello World"); // puts "put string" also prints a newline

  printf("The answer is %d\n",42); // "print formatted"

  putchar('\n'); // Print a single character

  fprintf(stdout,"Yes\n"); // print to a file - in this case standard out

  // All of the above are well known C standard library functions
  // Under the covers, the actual writing requires a system call.
  // Linux supports the POSIX API. All of the above ultimately call write: 

  // The system call to send 5 bytes to file descriptor #1 (standout output)
  write(1, "POSIX", 5 );
  write(1, " ROCKS", strlen(" ROCKS"));
  return 42;
}
