CFLAGS=-Wall -std=gnu99 -pthread -g
CC=gcc
PROGS:=$(patsubst %.c,%,$(wildcard *.c))

all : $(PROGS) 

% : %.c
	$(CC) $(CFLAGS) $(CPPFLAGS) $< -o $@

clean : 
	rm -rf $(PROGS) *.dSYM

